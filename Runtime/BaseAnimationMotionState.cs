using System.IO;
using UnityEditor;
using UnityEngine;

namespace StateMotion.Tool
{
#if UNITY_EDITOR
    [CustomEditor(typeof(BaseAnimationMotionState), true)]
    public class E_BaseCustomAnimationState : Editor
    {
        UnityEditor.Animations.StateMachineBehaviourContext[] context;
        private UnityEditor.Animations.AnimatorState _state;
        private BaseAnimationMotionState _behaviour;
        private static string _dataAssetName;
        private StateAnimationData _data;
        private const string PATH = "Assets/StateMotionToolData/";

        private void Awake()
        {
            _behaviour = target as BaseAnimationMotionState;
            context = UnityEditor.Animations.AnimatorController.FindStateMachineBehaviourContext(
                target as StateMachineBehaviour);

            if (context.Length <= 0) return;

            _dataAssetName = context[0].animatorController.name + nameof(StateAnimationData) + ".asset";

            _data = AssetDatabase.LoadAssetAtPath<StateAnimationData>(PATH + _dataAssetName);
            if (_data != null)
                _behaviour.dataObjectIsCreated = true;
        }

        public override void OnInspectorGUI()
        {
            if (_behaviour.dataObjectIsCreated == false)
                if (GUILayout.Button(nameof(CreateAnimatorData)))
                    CreateAnimatorData();

            if (_behaviour.dataObjectIsCreated)
                if (GUILayout.Button(nameof(SaveStateInfo)))
                    SaveStateInfo();

            base.OnInspectorGUI();
        }

        private void SaveStateInfo()
        {
            if (context != null && context.Length > 0)
            {
                _state = context[0].animatorObject as UnityEditor.Animations.AnimatorState;
                if (_state != null && _state.motion != null)
                {
                    _state.name = _behaviour.MotionClipName;
                    _behaviour.motionClipHash = _state.nameHash;

                    AddInfo(new MotionClipInfo(_state.nameHash, _state.name, _state.motion.averageDuration));
                }
                else
                    Debug.LogWarning("State or state motion doesn't exist.");
            }
        }

        private void CreateAnimatorData()
        {
            _data = CreateInstance<StateAnimationData>();

            _behaviour.dataObjectIsCreated = true;
            _data.name = _dataAssetName;
            string folder = "StateMotionToolData";
            if (Directory.Exists(PATH) == false)
                AssetDatabase.CreateFolder("Assets", folder);

            AssetDatabase.CreateAsset(_data, $"{PATH}/{_dataAssetName}");
            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();

            Selection.activeObject = _data;
            Debug.Log($"created data asset: {_data.name}");
        }

        private void AddInfo(MotionClipInfo clipInfo)
        {
            if (_data == null)
            {
                _behaviour.dataObjectIsCreated = false;
                return;
            }

            _data.AddClipInfo(clipInfo);
            EditorUtility.SetDirty(_data);
        }
    }
#endif


    public abstract class BaseAnimationMotionState : StateMachineBehaviour
    {
#if UNITY_EDITOR
        [HideInInspector] [SerializeField] public bool dataObjectIsCreated;
#endif
        [SerializeField] protected MotionStateEventData[] stateEventData;
        [InspectorReadOnlyAttribute] public int motionClipHash;
        public abstract string MotionClipName { get; }
        private MotionStateAnimatorReceiver animatorStateReceiver;
        private MotionEvent _motionEvent;


        public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, animatorStateInfo, layerIndex);

            animatorStateReceiver = animator.GetComponent<MotionStateAnimatorReceiver>();
            for (int i = 0; i < stateEventData.Length; i++)
            {
                stateEventData[i].customTimeRised = false;

                if (stateEventData[i].stateType == MotionStateEventType.Start)
                    animatorStateReceiver.broadcastMotionEvent?.Invoke(new MotionEvent(stateEventData[i].stateType,
                        motionClipHash));
            }
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            base.OnStateUpdate(animator, animatorStateInfo, layerIndex);

            for (int i = 0; i < stateEventData.Length; i++)
            {
                if (stateEventData[i].customTimeRised ||
                    stateEventData[i].stateType != MotionStateEventType.CustomTimePosition)
                    continue;

                if (animatorStateInfo.normalizedTime > stateEventData[i].eventNormalizedTime)
                {
                    stateEventData[i].customTimeRised = true;
                    animatorStateReceiver.broadcastMotionEvent?.Invoke(new MotionEvent(stateEventData[i].stateType,
                        motionClipHash));
                }
            }
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            base.OnStateExit(animator, animatorStateInfo, layerIndex);

            for (int i = 0; i < stateEventData.Length; i++)
            {
                if (stateEventData[i].stateType == MotionStateEventType.Exit)
                    animatorStateReceiver.broadcastMotionEvent?.Invoke(new MotionEvent(stateEventData[i].stateType,
                        motionClipHash));
            }
        }
    }
}