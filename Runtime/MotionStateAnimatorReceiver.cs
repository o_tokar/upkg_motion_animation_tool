﻿using System;
using UnityEngine;

namespace StateMotion.Tool
{
    [RequireComponent(typeof(Animator))]
    public class MotionStateAnimatorReceiver : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private StateAnimationData stateAnimationData = default;

        public Animator Animator => animator;
        public Action<MotionEvent> broadcastMotionEvent;
        public StateAnimationData Data => stateAnimationData;
        public Action OnRemove;


        private void OnDestroy()
        {
            OnRemove?.Invoke();
        }
    }
}