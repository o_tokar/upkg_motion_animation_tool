using System.Collections.Generic;
using UnityEngine;

namespace StateMotion.Tool
{
    public class StateAnimationData : ScriptableObject
    {
        [SerializeField] public List<MotionClipInfo> clipInfos = new List<MotionClipInfo>();

        public void AddClipInfo(MotionClipInfo clipInfo)
        {
            for (int i = 0; i < clipInfos.Count; i++)
            {
                if (clipInfos[i].Hash != clipInfo.Hash) continue;
                Debug.Log($"clip info with hash: {clipInfo.Hash} already exist. And was re-write successfully.");
                clipInfos[i] = clipInfo;
                return;
            }

            clipInfos.Add(clipInfo);
        }
    }
}