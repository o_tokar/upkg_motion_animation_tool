using System;

namespace StateMotion.Tool

{
    public class MotionEvent : IEquatable<MotionEvent>
    {
        public MotionEvent(MotionStateEventType motionStateType, int animationStateHash)
        {
            MotionStateType = motionStateType;
            AnimationStateHash = animationStateHash;
        }

        public int AnimationStateHash { get; private set; }
        public MotionStateEventType MotionStateType { get; private set; }

        public bool Equals(MotionEvent other)
        {
            if (other == null)
                return false;
            if (AnimationStateHash != other.AnimationStateHash)
                return false;
            if (MotionStateType != other.MotionStateType)
                return false;

            // UnityEngine.Debug.Log($" equals: {nameof(MotionEvent)} {ret}");
            return true;
        }

        public override int GetHashCode()
        {
            int hash = 0;
            hash ^= this.AnimationStateHash;
            hash ^= this.MotionStateType.GetHashCode();

            // UnityEngine.Debug.Log($" hash: {nameof(MotionEvent)} {hash}");
            return hash;
        }

        public override string ToString()
        {
            return $"{nameof(AnimationStateHash)}: {AnimationStateHash}, {nameof(MotionStateType)}: {MotionStateType}";
        }
    }
}