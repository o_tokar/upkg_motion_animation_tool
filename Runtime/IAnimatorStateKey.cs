namespace StateMotion.Tool
{
    public interface IAnimatorStateKey<out T> where T : struct
    {
        T ActiveAnimationStateKey { get;}
    }
}    