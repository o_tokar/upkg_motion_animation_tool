using UnityEngine;

namespace StateMotion.Tool
{
#if UNITY_EDITOR
    using UnityEditor;

    [CustomEditor(typeof(MotionStateEventData), true)]
    public class E_MotionStateEventData : Editor
    {
    }
#endif
    
    [System.Serializable]
    public class MotionStateEventData
    {
        [SerializeField] public MotionStateEventType stateType;

        /// <summary> Custom animation execution normalize time. </summary>
        [SerializeField] [Range(0, 1)] public float eventNormalizedTime;

        /// <summary> On a Custom time animation already executed. </summary>
        [HideInInspector] public bool customTimeRised;
    }
}