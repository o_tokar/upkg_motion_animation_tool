namespace StateMotion.Tool
{
    public enum MotionStateEventType
    {
        Start,
        CustomTimePosition,
        Exit
    }
}