using System;
using UnityEngine;

namespace StateMotion.Tool
{
    [Serializable]
    public class MotionClipInfo
    {
        [HideInInspector] [SerializeField] private string editorElementName;
        [ShowOnly] [SerializeField] private float duration;
        [ShowOnly] [SerializeField] private string name;
        [ShowOnly] [SerializeField] private int hash;

        public MotionClipInfo(int hash, string name, float duration)
        {
            this.hash = hash;
            this.name = name;
            this.duration = Mathf.Round(duration * 100F) / 100F;
            editorElementName = $"{this.name}: {this.duration} sec.";
        }

        public MotionClipInfo()
        {
        }


        /// <summary> Durations clip in sec. </summary>
        public float Duration => duration;

        /// <summary> Name based-on animation state key. </summary>
        public string Name => name;

        /// <summary> Hash based-on animation state string-key. </summary>
        public int Hash => hash;

        public override string ToString()
        {
            return $"{nameof(Duration)}: {Duration}, {nameof(Name)}: {Name}, {nameof(Hash)}: {Hash}";
        }
    }
}