using System;
using System.Collections.Generic;
using System.Linq;

namespace StateMotion.Tool
{
    public class AnimatorMotionStateAdapter<T> where T : struct, IConvertible
    {
        private Dictionary<MotionEvent, Action> _eventActions = new Dictionary<MotionEvent, Action>();
        private Dictionary<int, MotionClipInfo> _clipData = new Dictionary<int, MotionClipInfo>();
        private Dictionary<T, int> _keyOfHash = new Dictionary<T, int>();
        private MotionStateAnimatorReceiver _receiver;
        /// <summary> Constructor </summary>
        public AnimatorMotionStateAdapter(MotionStateAnimatorReceiver receiver)
        {
            if (receiver.Data == null)
            {
                UnityEngine.Debug.LogError(
                    $"{nameof(StateAnimationData)} not set to {nameof(MotionStateAnimatorReceiver)}");
                return;
            }

            _receiver = receiver;
            
            var data = _receiver.Data;
            _receiver.OnRemove += Clear;
            var values = Enum.GetValues(typeof(T)).Cast<T>().ToArray();
            for (int i = 0; i < values.Length; i++)
            {
                var stringKey = values[i].ToString();
                var hash = UnityEngine.Animator.StringToHash(stringKey);
                _keyOfHash.Add(values[i], hash);
            }

            for (int i = 0; i < data.clipInfos.Count; i++)
                _clipData.Add(data.clipInfos[i].Hash, data.clipInfos[i]);

            _receiver.broadcastMotionEvent = BroadcastAnimationEvent;
        }

        /// <summary>
        /// Creating animation event associate with Editor representation.  
        /// </summary>
        /// <param name="eType">State event type</param>
        /// <param name="key">T - animation - key </param>
        /// <param name="action">Invokes on event execute.</param>
        /// TODO: wrap EDITOR's defines.
        public void RegisterAnimationStateEvent(MotionStateEventType eType, T key, Action action)
        {
            if (_keyOfHash.TryGetValue(key, out var hash))
            {
                try
                {
                    _eventActions.Add(new MotionEvent(eType, hash), action);
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogError($"The same event {eType} with key: {key} already exist.");
                }
            }
            else
                UnityEngine.Debug.LogError($"Hash not found by key {key}");
        }

        /// <summary> Play animation motion state by key. </summary>
        public void Play(T key, int layer = -1)
        {
            int hash = 0;
            hash = GetStateHash(key);
            
            _receiver.Animator.Play(hash, layer);
        }
        
        /// <summary> Set trigger animation state by hashed string (Editor's trigger). </summary>
        public void SetTrigger(int hash)
        {
            _receiver.Animator.SetTrigger(hash);
        }
        /// <summary> Set bool parameter. </summary>
        /// <param name="hash">animation hash key based</param>
        /// <param name="state">animation bool parameter</param>
        public void SetBool(int hash, bool state)
        {
            _receiver.Animator.SetBool(hash, state);
        }

        /// <summary> Return animation durations in sec by key. </summary>
        /// <param name="key">T animation key</param>
        /// <returns>Clip length.</returns>
        public float GetClipDuration(T key)
        {
            if (_keyOfHash.TryGetValue(key, out int hash))
                if (_clipData.TryGetValue(hash, out MotionClipInfo value))
                    return _clipData[hash].Duration;
                else
                    UnityEngine.Debug.LogError($"Clip data not found by key {key}");
            else 
                UnityEngine.Debug.LogError($"Hash not found by key {key}");
            return 0;
        }

        /// <summary> Get animation state hash by key. </summary>
        /// <param name="key">animation key</param>
        public int GetStateHash(T key)
        {
            if (_keyOfHash.TryGetValue(key, out var hash)) return hash;

            UnityEngine.Debug.LogError($"Hash not found by key {key}");
            return 0;
        }

        /// <summary> Broadcast animation event. </summary>
        /// <param name="eventData">data</param>
        private void BroadcastAnimationEvent(MotionEvent eventData)
        {
            UnityEngine.Debug.Log($"Received animation event: {eventData}");

            if (_eventActions.TryGetValue(eventData, out var action))
                action?.Invoke();
            else
                UnityEngine.Debug.LogWarning(
                    $"No associated event data {eventData}. \n Create event or remove Editor representation from animation state.");
        }

        private void Clear()
        {
            _clipData.Clear();
            _eventActions.Clear();
            _keyOfHash.Clear();
        }
    }
}